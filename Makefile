# Conway's Game of Life
# 2020 Ciaran Anscomb

all: life.cas life.wav life16.cas life16.wav
.PHONY: all

PACKAGE = life
VERSION = 1.0
distdir = $(PACKAGE)-$(VERSION)

#---------------------------------------------------------------

ASM6809 = asm6809 -v
BIN2CAS = bin2cas.pl
B2CFLAGS = -r 48000
CFLAGS += -std=c99
CLEAN =
EXTRA_DIST =

#---------------------------------------------------------------

# Build tools

CLEAN += tile2bin
tile2bin: CFLAGS += $(shell sdl2-config --cflags)
tile2bin: LDLIBS += $(shell sdl2-config --libs) -lSDL2_image

#---------------------------------------------------------------

# Common build rules

%.bin: %.s
	$(ASM6809) -B -l $(<:.s=.lis) -s $(<:.s=.sym) -E $(<:.s=.exp) -o $@ $<

%.dz: %
	dzip -c $< > $@

#---------------------------------------------------------------

# Resources

PATTERNS = acorn.png gun.png

CLEAN += patterns.s
patterns.s: tile2bin bin2s.pl
patterns.s: $(PATTERNS)

patterns.s:
	@echo "acorn" > $@
	@echo "		fcb 3,1" >> $@
	./tile2bin -r acorn.png | ./bin2s.pl 1 >> $@
	@echo "gun" >> $@
	@echo "		fcb 9,5" >> $@
	./tile2bin -r gun.png | ./bin2s.pl 5 >> $@
	@echo "gun2" >> $@
	@echo "		fcb 9,5" >> $@
	./tile2bin -r -X -Y gun.png | ./bin2s.pl 5 >> $@

# Main binary

CLEAN += life.bin life.lis
life.bin: patterns.s
life.bin: life.s
	$(ASM6809) -C -d HIRES -l life.lis -o $@ $<

CLEAN += life16.bin life16.lis
life16.bin: patterns.s
life16.bin: life.s
	$(ASM6809) -C -l life16.lis -o $@ $<

# Final tape images

CLEAN += life.cas life.wav
life.cas life.wav: life.bin
	$(BIN2CAS) $(B2CFLAGS) --cue --eof-data -o $@ -n LIFE -C $<

CLEAN += life16.cas life16.wav
life16.cas life16.wav: life16.bin
	$(BIN2CAS) $(B2CFLAGS) --cue --eof-data -o $@ -n LIFE -C $<

#---------------------------------------------------------------

# Packaging

CLEAN += $(distdir).tar $(distdir).tar.gz
dist: $(EXTRA_DIST)
	git archive --format=tar --output=./$(distdir).tar --prefix=$(distdir)/ HEAD
	tar -r -f ./$(distdir).tar --owner=root --group=root --mtime=./$(distdir).tar --transform "s,^,$(distdir)/," $(EXTRA_DIST)
	gzip -f9 ./$(distdir).tar
.PHONY: dist

#---------------------------------------------------------------

clean:
	rm -f $(CLEAN)
.PHONY: clean

clena: clean
	@echo "How much clena do you want it?"
.PHONY: clena
